<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
set_time_limit(999999);
function fetchAndProcessUrls(array $urls, callable $f) {

    $multi = curl_multi_init();
    $reqs  = [];

    foreach ($urls as $url) {
        $req = curl_init();
        curl_setopt($req, CURLOPT_URL, $url);
        curl_setopt($req, CURLOPT_HEADER, 0);
        curl_multi_add_handle($multi, $req);
        $reqs[] = $req;
    }

    // While we're still active, execute curl
    $active = null;

    // Execute the handles
    do {
        $mrc = curl_multi_exec($multi, $active);
    } while ($mrc == CURLM_CALL_MULTI_PERFORM);

    while ($active && $mrc == CURLM_OK) {
        if (curl_multi_select($multi) != -1) {
            do {
                $mrc = curl_multi_exec($multi, $active);
            } while ($mrc == CURLM_CALL_MULTI_PERFORM);
        }
    }

    // Close the handles
    foreach ($reqs as $req) {
        $f(curl_multi_getcontent($req));
        curl_multi_remove_handle($multi, $req);
    }
    curl_multi_close($multi);
}
zapros('http://localhost/clear.php');

$urlArray1 = ['http://localhost/zakup.php', 'http://localhost/zakup2.php',  'http://localhost/ssp.php', 'http://localhost/region.php', 'http://localhost/zadolfssp.php'];
fetchAndProcessUrls($urlArray1, function($requestData) { });

$urlArray2 = ['http://localhost/zadol1.php', 'http://localhost/zadol2.php', 'http://localhost/zadol3.php', 'http://localhost/zadol4.php', 'http://localhost/sp1.php', 'http://localhost/sp2.php', 'http://localhost/sp3.php', 'http://localhost/id.php', 'http://localhost/old.php'];
fetchAndProcessUrls($urlArray2, function($requestData) { });

zapros('http://localhost/sum.php');

function zapros($link) 
{ 
	$ch = curl_init();
	$linkV = $link;
	curl_setopt($ch, CURLOPT_URL, $linkV); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
	$curl_result = curl_exec($ch); 
	//echo $link; 
	//echo '<br>';
	//echo $curl_result; 
	return $curl_result; 
} 
?>