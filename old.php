<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
set_time_limit(999999);
mb_internal_encoding("UTF-8");

$request_params1 = array( 
		'param' => 'ЗадолженностьПоОбъектам?ТипОбъекта=Услуги&НомерСтрокиНачало=',
		'count' => '&Количество=',
		'num' => 5000,
		'potok' =>3,
		'dir' => 'E:\\Переходная\\BI\\Выгрузки 1С\\Задолженность СД\\',
		'start'=> 0,
		'name' => 'СД_Задолженность_',
		'link' => 'http://192.168.190.4/legal/hs/oData/'
	); 

$request_params2 = array( 
		'param' => 'ЗадолженностьПоОбъектам?ТипОбъекта=Залоги&НомерСтрокиНачало=',
		'count' => '&Количество=',
		'num' => 5000,
		'potok' =>3,
		'start'=> 0,
		'dir' => 'E:\\Переходная\\BI\\Выгрузки 1С\\Задолженность ПР\\',
		'name' => 'ПР_Задолженность_',
		'link' => 'http://192.168.190.4/legal/hs/oData/'
	); 

$request_params3 = array( 
		'param' => 'ЗадолженностьПоОбъектам?ТипОбъекта=ИсполнительныеДокументы&НомерСтрокиНачало=',
		'count' => '&Количество=',
		'num' => 5000,
		'potok' =>1,
		'start'=> 0,
		'dir' => 'E:\\Переходная\\BI\\Выгрузки 1С\\Задолженность ИД\\',
		'name' => 'ИД_Задолженность_',
		'link' => 'http://192.168.190.4/legal/hs/oData/'
	); 

$dir1 = count(scandir($request_params1['dir']));
$dir2 = count(scandir($request_params2['dir']));
$dir3 = count(scandir($request_params3['dir']));
$count = 100;
while(true)
{
	if ($count == 0)
		break;
	
	$zadol1 = 'http://localhost/test.php?'.http_build_query($request_params1);
	$request_params1['start'] = $request_params1['start'] + $request_params1['num'];
	$zadol4 = 'http://localhost/test.php?'.http_build_query($request_params1);
	$request_params1['start'] = $request_params1['start'] + $request_params1['num'];
	$zadol5 = 'http://localhost/test.php?'.http_build_query($request_params1);
	$zadol2 = 'http://localhost/test.php?'.http_build_query($request_params2);
	$request_params2['start'] = $request_params2['start'] + $request_params2['num'];
	$zadol6 = 'http://localhost/test.php?'.http_build_query($request_params2);
	$request_params2['start'] = $request_params2['start'] + $request_params2['num'];
	$zadol7 = 'http://localhost/test.php?'.http_build_query($request_params2);
	$zadol3 = 'http://localhost/test.php?'.http_build_query($request_params3);
	
	$urlArray1 = [$zadol1,$zadol2,$zadol3,$zadol4,$zadol5,$zadol6,$zadol7];
	fetchAndProcessUrls($urlArray1, function($requestData) { });
	
	if ($dir1 != count(scandir($request_params1['dir'])))
	{
		$dir1 = count(scandir($request_params1['dir']));
		$request_params1['start'] = $request_params1['start'] + $request_params1['num'];
	}
	else
		$dir2 == 0;
	if ($dir2 != count(scandir($request_params2['dir'])))
	{
		$dir2 = count(scandir($request_params2['dir']));
		$request_params2['start'] = $request_params2['start'] + $request_params2['num'];
	}
	else
		$dir2 == 0;
	if ($dir3 != count(scandir($request_params3['dir'])))
	{
		$dir3 = count(scandir($request_params3['dir']));
		$request_params3['start'] = $request_params3['start'] + $request_params3['num'];
	}
	else
		$dir3 == 0;
	$count = $count - 1;
}


function zapros($link) 
{ 
	$ch = curl_init();
	$linkV = $link;
	curl_setopt($ch, CURLOPT_URL, $linkV); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
	curl_setopt($ch, CURLOPT_USERPWD, "ws:ws");
	$curl_result = curl_exec($ch); 
	//echo $link; 
	//echo '<br>';
	//echo $curl_result; 
	return $curl_result; 
} 
function fetchAndProcessUrls(array $urls, callable $f) {

    $multi = curl_multi_init();
    $reqs  = [];

    foreach ($urls as $url) {
        $req = curl_init();
        curl_setopt($req, CURLOPT_URL, $url);
        curl_setopt($req, CURLOPT_HEADER, 0);
        curl_multi_add_handle($multi, $req);
        $reqs[] = $req;
    }

    // While we're still active, execute curl
    $active = null;

    // Execute the handles
    do {
        $mrc = curl_multi_exec($multi, $active);
    } while ($mrc == CURLM_CALL_MULTI_PERFORM);

    while ($active && $mrc == CURLM_OK) {
        if (curl_multi_select($multi) != -1) {
            do {
                $mrc = curl_multi_exec($multi, $active);
            } while ($mrc == CURLM_CALL_MULTI_PERFORM);
        }
    }

    // Close the handles
    foreach ($reqs as $req) {
        $f(curl_multi_getcontent($req));
        curl_multi_remove_handle($multi, $req);
    }
    curl_multi_close($multi);
}
?>